Assumptions:

!!!! For congestion controll assume TCP is working as a blackbox. !!!!

!!!! C.masterchunks file has the exact following format for the first two lines (copied from PDF): !!!

File:	/tmp/C.tar	
Chunks:	

Bugs:
1) 	There is a problem with sending packages to the correct clients. Please run both peers as denoted by:
	./peer -p /tmp/nodes.map -c /tmp/A.haschunks -f /tmp/C.masterchunks -m 4 -i 1
	./peer -p /tmp/nodes.map -c /tmp/B.haschunks -f /tmp/C.masterchunks -m 4 -i 2	

	Run the GET /tmp/B.chunks /tmp/newB.tar command in the first peer and all the results should pop in the second peer. Hopefully will manage to fix this.

2) 	"config.output" does not work properly. /tmp/newB.tar is hardcoded as output path.

3) 	Even though the newB.tar file is opened AND has some data transferred to it the complete package is not successfully transfered. The problem seems to lie in the fact that padding is not writter into the buffer (nor the actual newB.tar file)

4) 	TCP currently doesnt have detection for package buffering / dropping / resending packages. THIS HOPEFULLY WILL BE FIXED. Actively working on it.

5) 	On that note the implemented congestion controll assumes the TCP sliding window is acting like a black 	box. I have denoted certain places with PACKAGE_DROPPED macro. Currently it is set to 0, however my logic would run if TCP detects a package drop.

6) Apologies for the misconfiguration of the git repo. Will try to fix it.

Please find CongestionControll.pdf to see my congestion control implementation in action in a controlled environment.
